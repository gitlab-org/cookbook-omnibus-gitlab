# Cookbook Name:: omnibus-gitlab
# Recipe:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'
require 'chef-vault'

describe 'omnibus-gitlab::default' do
  context 'with basic gitlab.rb' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.normal['omnibus-gitlab']['gitlab_rb']['external_url'] = 'http://herpderp-extern.com'
        node.normal['omnibus-gitlab']['gitlab_rb']['pages_external_url'] = 'http://herpderp-pages.com'
        node.normal['omnibus-gitlab']['gitlab_rb']['logging']['udp_log_shipping_host'] = '127.0.0.1'
        node.normal['omnibus-gitlab']['gitlab_rb']['gitlab_exporter']['listen_address'] = '0.0.0.0'
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.not_to raise_error
    end

    it 'creates gitlab.rb' do
      expect(chef_run).to create_template('/etc/gitlab/gitlab.rb').with(
        mode: '0600'
      )
    end

    it 'sets the logging name' do
      expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
        expect(c).to include("external_url 'http://herpderp-extern.com'")
        expect(c).to include("pages_external_url 'http://herpderp-pages.com'")
        expect(c).to include("logging['udp_log_shipping_hostname'] = 'fauxhai.local'")
        expect(c).to include("gitlab_exporter['listen_address'] = \"0.0.0.0\"")
      }
    end

    it 'executes reconfigure' do
      expect(chef_run.template('/etc/gitlab/gitlab.rb')).to notify('execute[gitlab-ctl reconfigure]').to(:run)
      expect(chef_run.execute('gitlab-ctl reconfigure')).to do_nothing
      expect(chef_run.execute('gitlab-ctl reconfigure')).to have_attributes(
        environment: { 'CONFIG' => '' }
      )
    end

    context 'when `skip_auto_reconfigure` is set' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['skip_auto_reconfigure'] = true
        end.converge(described_recipe)
      end

      it 'creates the skip file' do
        expect(chef_run).to create_file('/etc/gitlab/skip-auto-reconfigure')
      end
    end

    context 'when CloudFlare Origin Pull is enforced' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['cloudflare']['origin_pull']['enforced'] = true
        end.converge(described_recipe)
      end

      it 'creates the certificate' do
        expect(chef_run).to create_cookbook_file('/etc/gitlab/ssl/cf-origin-pull.pem').with(mode: '0600')
      end

      it 'notifies nginx to reload' do
        expect(chef_run.cookbook_file('/etc/gitlab/ssl/cf-origin-pull.pem')).to notify('bash[reload nginx configuration]').to(:run)
      end

      it 'sets nginx config' do
        expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
          expect(c).to include("nginx['ssl_verify_client'] = \"on\"")
          expect(c).to include("nginx['ssl_client_certificate'] = \"/etc/gitlab/ssl/cf-origin-pull.pem\"")
        }
      end
    end

    context 'when letsencrypt is true' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['gitlab_rb']['letsencrypt']['enable'] = true
        end.converge(described_recipe)
      end

      it 'removes the `ssl_certificate` options' do
        expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
          expect(c).not_to include("nginx['ssl_certificate']")
          expect(c).not_to include("nginx['ssl_certificate_key']")

          expect(c).not_to include("registry_nginx['ssl_certificate']")
          expect(c).not_to include("registy_nginx['ssl_certificate_key']")
        }
      end
    end

    context 'when package install is enabled' do
      before do
        allow(Dir).to receive(:exist?).with('/somedir').and_return(true)
      end

      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['package']['name'] = "gitlab-ee"
          node.normal['omnibus-gitlab']['package']['version'] = "11.9.8-ee.0"
          node.normal['omnibus-gitlab']['package']['prom_metric_file'] = '/somedir/omnibus_package.prom'
        end.converge(described_recipe)
      end

      it 'installs the gitlab-ee package with name and version' do
        expect(chef_run).to install_apt_package('gitlab-ee').with(
          version: "11.9.8-ee.0"
        )
      end

      it 'creates the metric file' do
        expect(chef_run).to create_file('/somedir/omnibus_package.prom').with(
          mode: '0644',
          content: "omnibus_package_install{enable=\"true\"} 1.0\n"
        )
      end
    end

    context 'when package install is not enabled' do
      before do
        allow(Dir).to receive(:exist?).with('/somedir').and_return(true)
      end

      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['package']['name'] = "gitlab-ee"
          node.normal['omnibus-gitlab']['package']['version'] = "11.9.8-ee.0"
          node.normal['omnibus-gitlab']['package']['enable'] = false
          node.normal['omnibus-gitlab']['package']['prom_metric_file'] = '/somedir/omnibus_package.prom'
          node.normal['packages'] = {
            'gitlab-ee': '11.9.7-ee.0'
          }
        end.converge(described_recipe)
      end

      it 'does not install the gitlab-ee package with name and version' do
        expect(chef_run).not_to install_apt_package('gitlab-ee').with(
          version: "11.9.8-ee.0"
        )
      end

      it 'creates the metric file' do
        expect(chef_run).to create_file('/somedir/omnibus_package.prom').with(
          mode: '0644',
          content: "omnibus_package_install{enable=\"false\"} 1.0\n"
        )
      end
    end

    context 'when package install is not enabled but the gitlab-ee package is not installed' do
      before do
        allow(Dir).to receive(:exist?).with('/somedir').and_return(true)
      end

      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['package']['name'] = "gitlab-ee"
          node.normal['omnibus-gitlab']['package']['version'] = "11.9.8-ee.0"
          node.normal['omnibus-gitlab']['package']['enable'] = false
          node.normal['omnibus-gitlab']['package']['prom_metric_file'] = '/somedir/omnibus_package.prom'
        end.converge(described_recipe)
      end

      it 'does install the gitlab-ee package with name and version' do
        expect(chef_run).to install_apt_package('gitlab-ee').with(
          version: "11.9.8-ee.0"
        )
      end

      it 'creates the metric file' do
        expect(chef_run).to create_file('/somedir/omnibus_package.prom').with(
          mode: '0644',
          content: "omnibus_package_install{enable=\"false\"} 1.0\n"
        )
      end
    end

    context 'when `skip_auto_reconfigure` is not set' do
      it 'deletes the skip file' do
        expect(chef_run).to delete_file('/etc/gitlab/skip-auto-reconfigure')
      end
    end

    # rubocop: disable Metrics/BlockLength
    context 'with SSL configuration' do
      let(:certificate) { 'fake-cert' }
      let(:private_key) { 'fake-key' }

      context 'when Gitaly SSL configuration is set' do
        let(:certificate_path) { '/etc/gitlab/ssl/gitaly.crt' }
        let(:key_path) { '/etc/gitlab/ssl/gitaly.key' }

        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['gitaly_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['gitaly_private_key'] = private_key
            node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration']['tls']['certificate_path'] = certificate_path
            node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration']['tls']['key_path'] = key_path
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file(certificate_path).with(
            group: 'git',
            content: certificate
          )
          expect(chef_run).to create_file(key_path).with(
            group: 'git',
            mode: '0640',
            content: private_key
          )
        end
      end

      context 'when Gitaly SSL file content is not set' do
        let(:certificate_path) { '/etc/gitlab/ssl/gitaly.crt' }
        let(:key_path) { '/etc/gitlab/ssl/gitaly.key' }

        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['gitaly_certificate'] = ''
            node.normal['omnibus-gitlab']['ssl']['gitaly_private_key'] = ''
            node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration']['tls']['certificate_path'] = certificate_path
            node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration']['tls']['key_path'] = key_path
          end.converge(described_recipe)
        end

        it 'does not create the certificate nor private key files' do
          expect(chef_run).not_to create_file(certificate_path)
          expect(chef_run).not_to create_file(key_path)
        end
      end

      context 'when Praefect SSL configuration is set' do
        let(:certificate_path) { '/etc/gitlab/ssl/praefect.crt' }
        let(:key_path) { '/etc/gitlab/ssl/praefect.key' }

        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['praefect_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['praefect_private_key'] = private_key
            node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['tls']['certificate_path'] = certificate_path
            node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['tls']['key_path'] = key_path
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file(certificate_path).with(
            group: 'git',
            content: certificate
          )
          expect(chef_run).to create_file(key_path).with(
            group: 'git',
            mode: '0640',
            content: private_key
          )
        end
      end

      context 'when Praefect SSL file content is not set' do
        let(:certificate_path) { '/etc/gitlab/ssl/praefect.crt' }
        let(:key_path) { '/etc/gitlab/ssl/praefect.key' }

        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['praefect_certificate'] = ''
            node.normal['omnibus-gitlab']['ssl']['praefect_private_key'] = ''
            node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['tls']['certificate_path'] = certificate_path
            node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['tls']['key_path'] = key_path
          end.converge(described_recipe)
        end

        it 'does not create the certificate nor private key files' do
          expect(chef_run).not_to create_file(certificate_path)
          expect(chef_run).not_to create_file(key_path)
        end
      end

      context 'when Praefect database client SSL configuration is set' do
        let(:sslcert_path) { '/etc/gitlab/ssl/praefect-database-client-cert.pem' }
        let(:sslkey_path) { '/etc/gitlab/ssl/praefect-database-client-key.pem' }
        let(:sslrootcert_path) { '/etc/gitlab/ssl/praefect-database-server-ca.pem' }
        let(:ca) { 'fake-ca' }
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['database']['sslcert'] = sslcert_path
            node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['database']['sslkey'] = sslkey_path
            node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['database']['sslrootcert'] = sslrootcert_path
            node.normal['omnibus-gitlab']['ssl']['praefect_database_client_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['praefect_database_client_key'] = private_key
            node.normal['omnibus-gitlab']['ssl']['praefect_database_server_ca'] = ca
          end.converge(described_recipe)
        end

        it 'creates the certificate, private key, and certificate authority files' do
          expect(chef_run).to create_file(sslcert_path).with(
            group: 'git',
            content: certificate
          )
          expect(chef_run).to create_file(sslkey_path).with(
            group: 'git',
            mode: '0640',
            content: private_key
          )
          expect(chef_run).to create_file(sslrootcert_path).with(
            group: 'git',
            mode: '0640',
            content: ca
          )
        end

        context 'but artifacts do not exist' do
          let(:chef_run) do
            ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
              node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['database']['sslcert'] = sslcert_path
              node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['database']['sslkey'] = sslkey_path
              node.normal['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['database']['sslrootcert'] = sslrootcert_path
            end.converge(described_recipe)
          end

          it 'does not create the certificate, private key, nor certificate authority files' do
            expect(chef_run).not_to create_file(sslcert_path)
            expect(chef_run).not_to create_file(sslkey_path)
            expect(chef_run).not_to create_file(sslrootcert_path)
          end
        end
      end

      context 'when Praefect database client SSL configuration is not set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
        end

        it 'does not add praefect database client connections to the configuration' do
          expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
            expect(c).to_not include('"sslcert"')
            expect(c).to_not include('"sslkey"')
            expect(c).to_not include('"sslrootcert"')
          }
        end
      end

      context 'when nginx SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/nginx.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/nginx.key').with(
            mode: '0600',
            content: private_key
          )
        end
      end

      context 'when mattermost SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['mattermost_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['mattermost_private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/mattermost-nginx.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/mattermost-nginx.key').with(
            mode: '0600',
            content: private_key
          )
        end
      end

      context 'when pages SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['pages_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['pages_private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/pages.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/pages.key').with(
            mode: '0600',
            content: private_key
          )
        end
      end

      context 'when registry SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['registry_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['registry_private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/registry.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/registry.key').with(
            mode: '0600',
            content: private_key
          )
        end
      end

      context 'when trusted certificates are set' do
        let(:certificate_2) { 'another-cert' }
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['trusted_certs'] = {
              'trusted.crt' => certificate,
              'another.crt' => certificate_2
            }
          end.converge(described_recipe)
        end

        it 'creates the certificate files' do
          expect(chef_run).to create_file('/etc/gitlab/trusted-certs/trusted.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/trusted-certs/another.crt').with(
            content: certificate_2
          )
        end
      end
    end
    # rubocop: enable Metrics/BlockLength

    context 'when Gitaly signing key is set' do
      let(:key) { 'fake-key' }
      let(:key_path) { '/etc/gitlab/gitaly/signing_key.ssh' }
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '20.04') do |node|
          node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration']['git']['signing_key'] = key_path
          node.normal['omnibus-gitlab']['ssh']['gitaly_signing_key'] = key
        end.converge(described_recipe)
      end

      it 'creates the signing key file' do
        expect(chef_run).to create_file(key_path).with(content: key)
      end
    end

    context 'when user ratelimit bypasses set' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
          node.normal['omnibus-gitlab']['user_ratelimit_bypasses'] = {
            12345 => { "name": "user1", "issue": "https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/12345" },
            98765 => { "name": "anotheruser", "issue": "https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/12346" },
            192873471 => { "name": "user2", "issue": "https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/12347" }
          }
        end.converge(described_recipe)
      end

      it 'generates the expected env var configuration' do
        expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
          expect(c).to include('"GITLAB_THROTTLE_USER_ALLOWLIST"=>"12345,98765,192873471"')
        }
      end
    end

    context 'when clickhouse is configured for read-write mode' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
          node.normal['omnibus-gitlab']['gitlab_rb']['gitlab-rails']['clickhouse_databases']['main'] = {
            'database': 'dbname',
            'url': 'https://clickhouse-endpoint',
            'username': 'username',
            'password': 'read-write-pw',
            'password_ro': 'read-only-pw'
          }
        end.converge(described_recipe)
      end

      it 'generates the expected clickhouse configuration with the read-write password' do
        expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
          expect(c).to include("gitlab_rails['clickhouse_databases'] = {\"main\"=>{\"database\"=>\"dbname\"")
          expect(c).to_not include('"password"=>"read-only-pw"')
          expect(c).to include('"password"=>"read-write-pw"')
        }
      end
    end

    context 'when clickhouse is configured for read-only mode' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
          node.normal['omnibus-gitlab']['gitlab_rb']['gitlab-rails']['clickhouse_databases']['main'] = {
            'database': 'dbname',
            'url': 'https://clickhouse-endpoint',
            'username': 'username',
            'password': 'read-write-pw',
            'password_ro': 'read-only-pw',
            'read_only': true
          }
        end.converge(described_recipe)
      end

      it 'generates the expected clickhouse configuration with the read-write password' do
        expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
          expect(c).to include("gitlab_rails['clickhouse_databases'] = {\"main\"=>{\"database\"=>\"dbname\"")
          expect(c).to include('"password"=>"read-only-pw"')
          expect(c).to_not include('"password"=>"read-write-pw"')
          expect(c).to_not include('"read_only"=>true')
        }
      end
    end

    context 'when a service is explicity disabled' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
          node.normal['omnibus-gitlab']['populate_disabled_services'] = populate_disabled_services

          node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['enable'] = true
          node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['socket_path'] = '/tmp/socket.unix'

          node.normal['omnibus-gitlab']['gitlab_rb']['gitlab-rails']['enable'] = false
          node.normal['omnibus-gitlab']['gitlab_rb']['gitlab-rails']['db_password'] = 'hunter1'

          node.normal['omnibus-gitlab']['gitlab_rb']['gitlab_exporter']['listen_address'] = '0.0.0.0'
        end.converge(described_recipe)
      end

      context 'when populate_disabled_services is enabled' do
        let(:populate_disabled_services) { true }

        it 'generate its configuration' do
          expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
            expect(c).to include("gitaly['enable'] = true")
            expect(c).to include("gitaly['socket_path'] = \"/tmp/socket.unix\"")
            expect(c).to include("gitlab_rails['enable'] = false")
            expect(c).to include("gitlab_rails['db_password'] = \"hunter1\"")
            expect(c).to include("gitlab_exporter['listen_address'] = \"0.0.0.0\"")
          }
        end
      end

      context 'when populate_disabled_services is disabled' do
        let(:populate_disabled_services) { false }

        it 'does not generate any extra configuration for it' do
          expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
            expect(c).to include("gitaly['enable'] = true")
            expect(c).to include("gitaly['socket_path'] = \"/tmp/socket.unix\"")

            expect(c).to include("gitlab_rails['enable'] = false")
            expect(c).not_to include("gitlab_rails['db_password'] = \"hunter1\"")

            expect(c).to include("gitlab_exporter['listen_address'] = \"0.0.0.0\"")
          }
        end
      end
    end

    describe 'gitaly storage path deduplication' do
      let(:node_name) { 'gitaly-09-stor-gstg.c.gitlab-gitaly-gstg-164c.internal' }
      let(:enable_deduplication) { true }
      let(:daily_maintenance) {
        {"daily_maintenance" => {
          "duration": "30m",
          "start_hour": 8,
          "start_minute": 45,
          "storages": [
            "default"
          ]
        }}
      }
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
          node.name(node_name)

          node.normal['omnibus-gitlab']['gitaly']['deduplicate_storage_paths'] = enable_deduplication

          node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['enable'] = true
          node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration']['storage'] = [
            {
              "name": "default",
              "path": "/var/opt/gitlab/git-data/repositories"
            },
            {
              "name": "nfs-file23",
              "path": "/var/opt/gitlab/git-data/repositories"
            },
            {
              "name": "gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal",
              "path": "/var/opt/gitlab/git-data/repositories"
            }
          ]
          node.normal['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration'].merge!(daily_maintenance)

          node.normal['omnibus-gitlab']['gitlab_rb']['git_data_dirs'] = {
            "default": {
              "path": "/var/opt/gitlab/gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal",
              "gitaly_address": "tcp://gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal:9999"
            },
            "nfs-file23": {
              "path": "/var/opt/gitlab/git-data-file23",
              "gitaly_address": "tcp://file-hdd-01-stor-gstg.c.gitlab-staging-1.internal:9999"
            },
            "gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal": {
              "path": "/var/opt/gitlab/gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal",
              "gitaly_address": "tcp://gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal:9999"
            }
          }
        end.converge(described_recipe)
      end

      context 'when it is disabled' do
        let(:enable_deduplication) { false }

        it 'keeps Gitaly configuration as-is' do
          expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
            expect(c).to include('{"name"=>"default", "path"=>"/var/opt/gitlab/git-data/repositories"}')
            expect(c).to include('{"name"=>"nfs-file23", "path"=>"/var/opt/gitlab/git-data/repositories"}')
            expect(c).to include('{"name"=>"gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal", "path"=>"/var/opt/gitlab/git-data/repositories"}')
            expect(c).to include('"daily_maintenance"=>{"duration"=>"30m", "start_hour"=>8, "start_minute"=>45, "storages"=>["default"]}')
          }
        end
      end

      shared_examples 'updating daily_maintenance storages list' do |storage|
        it 'updates daily_maintenance stoages list to have only the correct storage' do
          expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
            expect(c).to include('"daily_maintenance"=>{"duration"=>"30m", "start_hour"=>8, "start_minute"=>45, "storages"=>["' + storage + '"]}')
          }
        end
      end

      context 'when it is enabled' do
        let(:enable_deduplication) { true }

        context 'when node name matches the storage name' do
          let(:node_name) { 'gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal' }

          it 'includes only one storage entry for the matched storage name' do
            expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
              expect(c).to include('{"name"=>"gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal", "path"=>"/var/opt/gitlab/git-data/repositories"}')
              expect(c).not_to include('{"name"=>"default", "path"=>"/var/opt/gitlab/git-data/repositories"}')
              expect(c).not_to include('{"name"=>"nfs-file23", "path"=>"/var/opt/gitlab/git-data/repositories"}')
            }
          end

          it_behaves_like 'updating daily_maintenance storages list', 'gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal'
        end

        context 'when node name does not matche the storage name' do
          context 'when the node has an entry in git_data_dirs' do
            let(:node_name) { 'file-hdd-01-stor-gstg.c.gitlab-staging-1.internal' }

            it 'includes only one storage entry for the matched storage name' do
              expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
                expect(c).to include('{"name"=>"nfs-file23", "path"=>"/var/opt/gitlab/git-data/repositories"}')
                expect(c).not_to include('{"name"=>"gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal", "path"=>"/var/opt/gitlab/git-data/repositories"}')
                expect(c).not_to include('{"name"=>"default", "path"=>"/var/opt/gitlab/git-data/repositories"}')
              }
            end

            it_behaves_like 'updating daily_maintenance storages list', 'nfs-file23'
          end

          context 'when the node does not have an entry in git_data_dirs' do
            let(:node_name) { 'gitaly-behind-praefect' }

            it 'includes the default storage entry for the matched storage name' do
              expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
                expect(c).to include('{"name"=>"default", "path"=>"/var/opt/gitlab/git-data/repositories"}')
                expect(c).not_to include('{"name"=>"nfs-file23", "path"=>"/var/opt/gitlab/git-data/repositories"}')
                expect(c).not_to include('{"name"=>"gitaly-01-stor-gstg.c.gitlab-gitaly-gstg-164c.internal", "path"=>"/var/opt/gitlab/git-data/repositories"}')
              }
            end

            it_behaves_like 'updating daily_maintenance storages list', 'default'
          end
        end

        context 'when daily_maintenance is absent' do
          let(:daily_maintenance) { {} }

          it 'remains absent' do
            expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
              expect(c).not_to include('daily_maintenance')
            }
          end
        end
      end
    end
  end
end
